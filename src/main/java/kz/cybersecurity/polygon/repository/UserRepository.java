package kz.cybersecurity.polygon.repository;

import kz.cybersecurity.polygon.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findUserByUsername(String username);

    Optional<User> deleteByUsername(String username);
}

