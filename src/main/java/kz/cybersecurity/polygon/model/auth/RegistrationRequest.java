package kz.cybersecurity.polygon.model.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationRequest {

    /**
     * (Id) username для логина
     */
    private String username;

    /**
     * Пароль
     */
    private String password;

    /**
     * Имя
     */
    private String firstName;

    /**
     * Фамилия
     */
    private String lastName;

    /**
     * Почта
     */
    private String email;
}

