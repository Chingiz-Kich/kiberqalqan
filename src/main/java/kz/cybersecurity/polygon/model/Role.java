package kz.cybersecurity.polygon.model;

public enum Role {
    ADMIN, USER, GUEST
}
